#!/usr/bin/env python
# coding: utf-8

# In[56]:


from sklearn import tree
import pandas as pd
import pydotplus
from IPython.display import Image


# In[57]:


read_annual = pd.read_csv('Temperature.csv')
read_annual


# In[58]:


dummi_data = read_annual.iloc[:, 0:4].values
dummi_data


# In[59]:


read_annual = read_annual [['record_id', 'month','day', 'year','AverageTemperatureFahr','Climate','Season']]
read_annual


# In[60]:


one_cold_data = pd.get_dummies(read_annual)
one_cold_data


# In[61]:


clf = tree.DecisionTreeClassifier()
clf_train = clf.fit(one_cold_data, read_annual['Climate'])
clf_train = clf.fit(one_cold_data, read_annual['Season'])


# In[64]:


dot_data = tree.export_graphviz(clf_train, out_file=None, feature_names=list(one_cold_data.columns.values),
                               class_names=['Hot','Summer','Autumn','cold','Spring','Winter'],
                               rounded=True, filled=True)
graph = pydotplus.graph_from_dot_data(dot_data)
Image(graph.create_png())

